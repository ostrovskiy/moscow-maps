# maps-moscow
Приложения с картами административных округов москвы получаемых из nominatim, так же присутствует поддержка зон (по клику percentchart am5) и генерация рандомных точек и стилей внутри округов
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn dev 
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
