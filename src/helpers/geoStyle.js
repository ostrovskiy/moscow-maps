export function randomizeStyle() {
  return {
    weight: 1,
    color: '#' + Math.floor(Math.random() * 16777215).toString(16),
    fillOpacity: 0.4,
  }
}
