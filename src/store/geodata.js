import Vuex from 'vuex'
import Vue from 'vue'

import { randomizeStyle } from '@/helpers/geoStyle'

Vue.use(Vuex)

import axios from 'axios'

export default {
  state: {
    geojson: {},
  },

  mutations: {
    SET(state, payload) {
      const geojson = {
        type: 'FeatureCollection',
        features: payload.map((item) => ({
          type: 'Feature',
          geometry: item.geojson,
          properties: {
            place_id: item.place_id,
            name: item.display_name,
            style: randomizeStyle(),
          },
        })),
      }
      state.geojson = geojson
    },
  },

  actions: {
    async FETCH_GEO({ commit }) {
      try {
        const response = await axios.get(
          'https://nominatim.openstreetmap.org/search.php',
          {
            params: {
              q: 'moscow russia administrative',
              polygon_geojson: 1,
              format: 'json',
              limit: 255,
            },
          }
        )

        commit('SET', response.data)
      } catch (error) {
        console.log(error)
      }
    },
  },
  namespaced: true,
}
