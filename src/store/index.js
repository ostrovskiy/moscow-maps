import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import geoData from './geodata.js'

export default new Vuex.Store({
  modules: {
    geoData,
  },
})
